//textUart1.c
#include "STC15Wxx.h"
#include "textUart.h"

char UAR[20],temp,rec_flag = 0;
//初始化串口和定时1
static void Uart1_Init(void)                    //@11.0592MHZ
{
    //关闭全局中断
    EA=0;
    //配置串口1开放在(p3.6/RxD_2,p3.7/TxD_2)
    ACC = P_SW1;
    ACC &= ~(S1_S0 | S1_S1);
    ACC |= S1_S0;
    P_SW1 = ACC;
    //配置串口
    SCON = 0x50;//8位可变波特率
    //配置定时器
    AUXR |= 0x40; //定时1为1T模式
    AUXR &= 0xFE;//选择定时器1为波特率发生器
    TMOD = 0x00;//定时器1为模式0(16位自动重载)
    TH1 = (65536 - (FOSC/4/BAUD)); //设置波特率重装值
    TH1 = (65536 - (FOSE/4/BAUD))>>8;
    TR1 = 1;//定时器1开始启动
    ES = 1;//开启串口中断
    EA = 1;//开启全局中断
}

//发送单个字符
void send1_Byte(unsigned char c)
{
    SBUF = c;
    while(!TI);//发送完会自动置1
    TI=0;
}

//发送字符串
void Send1_String(char *s)
{
    while (*s)      //检测字符串结束标志
    {
        send1_Byte(*s++);
    }
}

//串口接收到的数据存入UAR数组中,当接收到0x0D时表示数据接收结束
void UART1_Interrupt() interrupt4       //串口中断函数接收程序
{
    static unsigned char i;
    if (RI)
    {
        RI = 0;
        temp = SBUF;
        UAR[i] = temp;
        i++;
        if (temp == 0x0D)
        {
            i = 0;
            rec_flag = 1;//定义接收完成标志位
        }
    }
}

//串口初始化并测试
void UART1_config()
{
    Uart1_Init();//初始化函数
    Send1_String("STC15W4K56S4\r\nUart is ok ! \r\n");
}
void uart1_printf(const char *fmt,...)
{
    //待研究
    va_list ap;
    char xdate string[500];
    va_start(ap,fmt);
    vsprintf(string,fmp,ap);
    Send1_String(string);
    va_end(ap);
}







