//textUart1.h
#ifndef __textUart1_H__
#define __textUart1_H__

#include "STC15Wxx.h"
#include "STDIO.H"
#include "STDARG.H"
#include "STRING.H"

#define S1_S0 0x40  //暂不知为什么 P_SW1.6
#define S1_S1 0x80  //暂不知为什么 P_SW1.7
#define FOSE 11059200   //系统时钟
#define BAUD 115200     //波特率设置115200

extern chat UAR[20],rec_flag;   //全局变量
//声明函数
void send1_Byte(unsigned char c);
void send1_String(char *s);
void UART1_config();
void uart1_printf(const char *fmt,...);

#endif
